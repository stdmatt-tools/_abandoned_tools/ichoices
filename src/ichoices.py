#!/usr/bin/env python3
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : ichoices.py                                                   ##
##  Project   : ichoices                                                      ##
##  Date      : Feb 21, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
import curses;
import sys;
import string;

from difflib import SequenceMatcher as SM;


##----------------------------------------------------------------------------##
## Constants / Globals                                                        ##
##----------------------------------------------------------------------------##
PROGRAM_NAME      = "ichoices";
PROGRAM_VERSION   = "1.0.0";
PROGRAM_COPYRIGHT = "2020";

KEY_BACKSPACE = [127, curses.KEY_BACKSPACE];
KEY_DELETE    = [330];
KEY_ENTER     = 10;

class Globals:
    screen = None;


##----------------------------------------------------------------------------##
## Curses Helpers                                                             ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def init_curses():
    Globals.screen = curses.initscr();
    Globals.screen.keypad(True);

    curses.noecho();
    curses.cbreak();

##------------------------------------------------------------------------------
def shutdown_curses():
    Globals.screen.keypad(True);
    curses.nocbreak();
    curses.echo();

    curses.endwin();


##----------------------------------------------------------------------------##
## Matching Helpers                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def fuzzy_match(items, curr_content):
    best_ratio   =  0;
    best_index   = -1;
    curr_content = curr_content.strip(" ");

    ## Fuzzy Match.
    for i in range(0, len(items)):
        ratio = SM(None, items[i], curr_content).ratio();
        if(ratio > best_ratio):
            best_ratio = ratio;
            best_index = i;

    if(best_index != -1 and best_ratio > 0.3):
        return best_index;

    ## Index Match.
    if(curr_content.isdigit()):
        value = int(curr_content);
        if(value >= 0 and value < len(items)):
            return value;

    ## Couldn't match...
    return -1;


##----------------------------------------------------------------------------##
## Types                                                                      ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
class Items_List:
    ##--------------------------------------------------------------------------
    def __init__(self, items, x, y, height):
        self.x                 = x;
        self.y                 = y;
        self.height            = height;
        self.items             = items;
        self.curr_selection    = 0;
        self.confirm_selection = False;

    ##--------------------------------------------------------------------------
    def update(self, ch):
        self.confirm_selection = False;
        ## Change Selection.
        if(self._is_item_change_key(ch)):
            delta     = self._get_movement_delta_from_key(ch);
            new_index = (self.curr_selection + delta) % len(self.items);
            self.curr_selection = new_index;

            return True;

        ## Confirm Selection.
        if(ch == KEY_ENTER):
            self.confirm_selection = True;
            return True;

        return False;

    ##--------------------------------------------------------------------------
    def display(self):
        items_count = len(self.items);
        curr_index  = self.curr_selection;
        max_lines   = self.height;

        start_index = 0;
        end_index   = max_lines;

        if(curr_index >= max_lines):
            start_index = (curr_index - max_lines) + 1;
            end_index   = min(max_lines, items_count) + start_index;

        for i in range(0, max_lines):
            ci   = i + start_index;
            item = "({0}) - {1}".format(ci, self.items[ci]);

            y = self.y + i;
            x = self.x;

            if(ci == self.curr_selection):
                Globals.screen.addstr(y, x, item, curses.A_REVERSE)
            else:
                Globals.screen.addstr(y, x, item);

    ##--------------------------------------------------------------------------
    def _is_item_change_key(self, key):
        return key == curses.KEY_UP \
            or key == curses.KEY_DOWN;

    ##--------------------------------------------------------------------------
    def _get_movement_delta_from_key(self, key):
        if(key == curses.KEY_UP):
            return -1;
        if(key == curses.KEY_DOWN):
            return +1;
        return 0;

##------------------------------------------------------------------------------
class Edit_Box:
    EDIT_BOX_PROMPT_STR = ":) ";

    ##--------------------------------------------------------------------------
    def __init__(self, x, y):
        self.x            = x;
        self.y            = y;
        self.content_str  = "";
        self.cursor_index = 0;

    ##--------------------------------------------------------------------------
    def update(self, ch):
        ## Cursor Movement.
        if(self._is_cursor_move_key(ch)):
            delta     = self._get_movement_delta_from_key(ch);
            self.cursor_index += delta;

            if(self.cursor_index < 0):
                self.cursor_index = 0;
            elif(self.cursor_index > len(self.content_str)):
                self.cursor_index = len(self.content_str);

            return True;

        ## Contents Deletion.
        if(ch in KEY_BACKSPACE or ch in KEY_DELETE):
            return self._delete_char(ch);

        ## Contents Input.
        if(chr(ch) in string.printable):
            return self._insert_char(ch);

        return False;

    ##--------------------------------------------------------------------------
    def display(self):
        ## Add the prompt...
        Globals.screen.move(self.y, self.x);
        Globals.screen.deleteln(); ## @XXX(stdmatt): We need to find a way to delete just the contents...
        Globals.screen.addstr(Edit_Box.EDIT_BOX_PROMPT_STR + self.content_str);

        ## Let the cursor at the correct position.
        Globals.screen.move(
            self.y,
            self.x + self.cursor_index + len(Edit_Box.EDIT_BOX_PROMPT_STR)
        );

    ##--------------------------------------------------------------------------
    def _is_cursor_move_key(self, key):
        return key == curses.KEY_LEFT \
            or key == curses.KEY_RIGHT;

    ##--------------------------------------------------------------------------
    def _get_movement_delta_from_key(self, key):
        if(key == curses.KEY_LEFT):
            return -1;
        if(key == curses.KEY_RIGHT):
            return +1;
        return 0;

    ##--------------------------------------------------------------------------
    def _insert_char(self, ch):
        ch = chr(ch);

        l = list(self.content_str);
        l.insert(self.cursor_index, ch);

        self.cursor_index += 1;
        self.content_str   = "".join(l);

        return True;

    ##--------------------------------------------------------------------------
    def _delete_char(self, ch):
        if(len(self.content_str) == 0):
            return False;

        delete_direction = 0;
        if(ch in KEY_BACKSPACE):
            delete_direction = -1;
        elif(ch in KEY_DELETE):
            delete_direction = +0;

        index_to_delete = (self.cursor_index + delete_direction);
        if(index_to_delete < 0 or index_to_delete >= len(self.content_str)):
            return False;

        ## @improve(stdmatt): We don't need to create a list and convert back to str.
        l = list(self.content_str);
        l.pop(index_to_delete);

        self.cursor_index += delete_direction;
        self.content_str   = "".join(l);

        return True;


##----------------------------------------------------------------------------##
## entry point                                                                ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def run():
    choices = sys.argv[1:];
    if(len(choices) == 0):
        return 0;

    selection = None;
    try:
        init_curses();

        choices_count = len(choices);
        lines_count   = (curses.LINES - 1); ## Last line is for the edit_box
        max_count     = min(choices_count, lines_count);

        edit_box   = Edit_Box  (x=0, y=max_count);
        items_list = Items_List(choices, x=0, y=0, height=max_count);

        while not items_list.confirm_selection:
            Globals.screen.erase();

            items_list.display();
            edit_box  .display();

            Globals.screen.refresh();
            ch = Globals.screen.getch();

            if(items_list.update(ch)):
                continue; ## The input ws consumed by the list.
            if(edit_box.update(ch)):
                index = fuzzy_match(items_list.items, edit_box.content_str);
                if(index != -1):
                    items_list.curr_selection = index;

        ## Set the output selection.
        selection = items_list.items[items_list.curr_selection];
    except KeyboardInterrupt as e:
        sys.exit(0);
    except Exception as e:
        raise e;
    finally:
        shutdown_curses();
        if(selection is not None):
            sys.stderr.write(selection);


if __name__ == "__main__":
    run();
