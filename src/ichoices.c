/*~---------------------------------------------------------------------------*/
/*                        _      _                 _   _                      */
/*                    ___| |_ __| |_ __ ___   __ _| |_| |_                    */
/*                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   */
/*                   \__ \ || (_| | | | | | | (_| | |_| |_                    */
/*                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   */
/*                                                                            */
/*  File      : ichoices.c                                                    */
/*  Project   : ichoices                                                      */
/*  Date      : Apr 13, 2020                                                  */
/*  License   : GPLv3                                                         */
/*  Author    : stdmatt <stdmatt@pixelwizards.io>                             */
/*  Copyright : stdmatt - 2020                                                */
/*                                                                            */
/*  Description :                                                             */
/*                                                                            */
/*---------------------------------------------------------------------------~*/

//----------------------------------------------------------------------------//
// Includes                                                                   //
//----------------------------------------------------------------------------//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ncurses.h>

#include "levenshtein.h"


//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
#define PROGRAM_VERSION "2.0.0"

#define MAX_STRING_SIZE 256

int const KEYS_BACKSPACE[] = {127, KEY_BACKSPACE, 330};
#define   KEYS_BACKSPACE_COUNT 3

#define   KEY_CONTROL_C 3
#define   KEY_ESC       27


#define min(a, b) (a < b) ? (a) : (b)
void
log_fatal(char *fmt)
{

}

//----------------------------------------------------------------------------//
// Curses                                                                     //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
struct {
    SCREEN *screen;
    WINDOW *window;
} Globals;

//------------------------------------------------------------------------------
void
curses_init()
{
    FILE *out = stdout;
    if(!isatty(fileno(stdout))) {
        out = fopen("/dev/tty", "w");
        if(!out) {
            log_fatal("Counln't open /dev/tty");
        }
        setbuf(out, NULL);
    }

    SCREEN *screen = newterm(NULL, out, stdin);
    set_term(screen);
    WINDOW *window = newwin(LINES, COLS, 0, 0);
    keypad(window, true);

    Globals.screen = screen;
    Globals.window = window;

    noecho();
    raw();
    // cbreak();
}

//------------------------------------------------------------------------------
void
curses_shutdown()
{
    if(Globals.screen) {
        noraw();
        // nocbreak();
        echo    ();
        endwin  ();

        delwin   (Globals.window);
        delscreen(Globals.screen);

        Globals.window = NULL;
        Globals.screen = NULL;
    }
}


//----------------------------------------------------------------------------//
// "UI" Widgets                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
typedef struct {
    int   x;
    int   y;
    int   height;
    char **items;
    int   items_count;
    int   curr_selection;
    bool  confirmed_selection;
} Items_List;

//------------------------------------------------------------------------------
bool
Items_List_update(Items_List *items_list, int key)
{
    items_list->confirmed_selection = false;
    // Change selection.
    if(key == KEY_UP || key == KEY_DOWN) {
        int delta         = (key == KEY_UP) ? -1 : 1;
        int new_selection = items_list->curr_selection + delta;
        if(new_selection < 0) {
            new_selection = items_list->items_count-1;
        } else if(new_selection >= items_list->items_count) {
            new_selection = 0;
        }
        items_list->curr_selection = new_selection;
        return true;
    }
    // Confirm selection.
    if(key == KEY_ENTER || key == '\n') {
        items_list->confirmed_selection = true;
        return true;
    }

    return false;
}

//------------------------------------------------------------------------------
void
Items_List_display(Items_List *items_list)
{
    int start_index = 0;
    int max_lines   = items_list->height;

    if(items_list->curr_selection >= max_lines) {
        start_index = (items_list->curr_selection - max_lines) + 1;
    }

    char buffer[MAX_STRING_SIZE] = {'\0'};
    for(int i = 0; i < max_lines; ++i) {
        int ci = i + start_index;
        sprintf(buffer, "(%d) - %s", ci, items_list->items[ci]);

        int x = items_list->x;
        int y = items_list->y + i;

        if(ci == items_list->curr_selection) {
            wattron(Globals.window, A_REVERSE);
            mvwaddstr(Globals.window, y, x, buffer);
            wattroff(Globals.window, A_REVERSE);
        } else {
            mvwaddstr(Globals.window, y, x, buffer);
        }
    }
}

//------------------------------------------------------------------------------
#define EDIT_BOX_PROMPT_STR ":) "
typedef struct
{
    int   x;
    int   y;
    char *content_str;
    int   cursor_index;
} Edit_Box;

//------------------------------------------------------------------------------
bool
Edit_Box_update(Edit_Box *edit_box, int key)
{
    int  len = strlen(edit_box->content_str);
    char buffer[MAX_STRING_SIZE] = {'\0'};

    // Change selection.
    if(key == KEY_LEFT || key == KEY_RIGHT) {
        int delta = (key == KEY_LEFT) ? -1 : 1;
        edit_box->cursor_index += delta;

        if(edit_box->cursor_index < 0) {
            edit_box->cursor_index = 0;
        } else if(edit_box->cursor_index > len) {
            edit_box->cursor_index = len;
        }

        return true;
    }
    // Contents Deletion.
    for(int i = 0; i < KEYS_BACKSPACE_COUNT; ++i) {
        if(key != KEYS_BACKSPACE[i]) {
            continue;
        }
        if(len == 0) {
            return false;
        }

        int delete_direction = 0;
        if(key == KEY_BACKSPACE) {
            delete_direction = -1;
        }
        int index_to_delete = (edit_box->cursor_index + delete_direction);
        if(index_to_delete < 0 || index_to_delete >= len) {
            return false;
        }
        // Mateus
        // 012345
        memmove(
            edit_box->content_str + index_to_delete,
            edit_box->content_str + index_to_delete + 1,
            len - index_to_delete
        );

        edit_box->cursor_index += delete_direction;

        return true;
    }
    // Contents Input.
    // @todo(stdmatt): Only printable...
    memmove(
        buffer,
        edit_box->content_str,
        edit_box->cursor_index
    );
    buffer[edit_box->cursor_index] = key;
    memmove(
        buffer,
        edit_box->content_str + edit_box->cursor_index,
        len - edit_box->cursor_index
    );

    memmove(edit_box->content_str, buffer, MAX_STRING_SIZE);
    ++edit_box->cursor_index;
}

//------------------------------------------------------------------------------
void
Edit_Box_display(Edit_Box *edit_box)
{
    WINDOW *w = Globals.window;

    int x = edit_box->x;
    int y = edit_box->y;
    int pl = strlen(EDIT_BOX_PROMPT_STR);

    wmove    (w, y, x);
    wdeleteln(w);
    mvwaddstr(w, y, x,      EDIT_BOX_PROMPT_STR  );
    mvwaddstr(w, y, x + pl, edit_box->content_str);

    // Let the cursor on the right position.
    wmove(w, y, x + edit_box->cursor_index + pl);
}



//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
int
fuzzy_match(char **strings, int strings_count, char *content)
{
    //
    // Fuzzy Matching.
    int best_index = -1;
    int best_ratio = 999;

    for(int i = 0; i < strings_count; ++i) {
        size_t result = levenshtein(strings[i], content);
        if(result <= best_ratio) {
            best_ratio = result;
            best_index = i;
        }
    }
    if(best_index != -1 && best_ratio <= 2) {
        return best_index;
    }

    //
    // Index Matching.
    char *endptr = NULL;
    long result  = strtol(content, &endptr, 10);
    if(!*endptr && result >= 0 && result < strings_count) {
        return result;
    }
    return -1;
}

//----------------------------------------------------------------------------//
// Entry Point                                                                //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    curses_init();

    int items_count = argc  - 1;
    int lines_count = LINES - 1;
    int max_items   = min(items_count, lines_count);
    int final_index = -1;

    Items_List items_list = {
        .x                   = 0,
        .y                   = 0,
        .height              = max_items,
        .items               = (argv+1),
        .items_count         = items_count,
        .curr_selection      = 0,
        .confirmed_selection = false
    };
    Edit_Box edit_box = {
        .x = 0,
        .y = max_items,
        .content_str = malloc(sizeof(char) * MAX_STRING_SIZE),
        .cursor_index = 0
    };

    while(!items_list.confirmed_selection) {
        werase(Globals.window);

        Items_List_display(&items_list);
        Edit_Box_display  (&edit_box  );

        wrefresh(Globals.window);
        int key = wgetch(Globals.window);
        if(key == KEY_CONTROL_C || key == KEY_ESC) {
            final_index = -1;
            curses_shutdown();
            exit(0);
        }

        if(Items_List_update(&items_list, key)) {
            final_index = items_list.curr_selection;
        } else if(Edit_Box_update(&edit_box, key)) {
            int best_index = fuzzy_match(
                items_list.items,
                items_list.items_count,
                edit_box.content_str
            );
            if(best_index != -1) {
                items_list.curr_selection = best_index;
                final_index               = best_index;
            }
        }
    }

    curses_shutdown();

    if(final_index != -1) {
        printf("%s\n", items_list.items[final_index]);
    }

    return 0;
}
