#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : build.sh                                                      ##
##  Project   : ichoices                                                      ##
##  Date      : Apr 13, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2020                                                ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

source /usr/local/src/stdmatt/shellscript_utils/main.sh

SCRIPT_DIR=$(pw_get_script_dir);
SRC_DIR=$(pw_abspath "${SCRIPT_DIR}/../src");
BUILD_DIR=$(pw_abspath "${SCRIPT_DIR}/../build");

rm    -rf "$BUILD_DIR";
mkdir     "$BUILD_DIR";

gcc -g ${SRC_DIR}/*.c -o "${BUILD_DIR}/ichoices" -lncurses;
